# Horizon patch
It is a patch for Horizon in Openstack Liberty release.
Patch is needed to fix bug with loading angular.js for modals widget.

## Required variables
- install_horizon_patch: if True patch will be installed, otherwise not,
- path_to_horizon: path to Horizon plugin (/usr/share/openstack-dashboard/)
- path_to_horizon_package: path to Horizon python package
- files_list: list of files that should be replaced
